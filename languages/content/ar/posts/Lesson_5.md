+++
title = "Lesson 5"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-04"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English                                                                     	| Arabic                              	| Transliteration                            	|
|-----------------------------------------------------------------------------	|-------------------------------------	|--------------------------------------------	|
| I                                                                           	| أنا                                 	| Ana                                        	|
| We                                                                          	| نحن                                 	| nahnu                                      	|
| You (m, s)<br>You (f, s)<br>You (m/f, d)<br>You (m, pl)<br>You (f, pl)      	| أنت<br>أنت<br>انتما<br>انتم<br>انتنّ 	| inta<br>inti<br>intuma<br>intum<br>intunna 	|
| He<br>She<br>They (m/f, d)<br>They (m, pl)<br>They (f, pl)                  	| هو<br>هي<br>هما<br>هم<br>هنّ         	| huwa<br>hiya<br>huma<br>hum<br>hunna       	|
| My                                                                          	| ي                                   	| ya                                         	|
| Our                                                                         	| نا                                  	| naa                                        	|
| Your (m, s)<br>Your (f, s)<br>Your (m/f, d)<br>Your (m, pl)<br>Your (f, pl) 	| ك<br>ك<br>كما<br>كم<br>كنّ           	| ka<br>ki<br>kuma<br>kum<br>kunna           	|
| His<br>Her<br>Their (m/f, d)<br>Their (m, pl)<br>Their (f, pl)              	| ه<br>ها<br>هما<br>هم<br>هنّ          	| ho/hi<br>haa<br>humaa<br>hum<br>hunna      	|
| Television<br>Televisions                                                   	| تلفاز<br>تلفازات                    	| tilfaz<br>tilfaazaat                       	|
| Newspaper<br>Newspapers                                                     	| جريدة<br>جرائد                      	| jareeda<br>jaraaid                         	|
| Computer<br>Computers                                                       	| حاسوب<br>حواسيب                     	| hasoob<br>hawasib                          	|
| Bicycle<br>Bicycles                                                         	| دراجة<br>دراجات                     	| darraaja<br>darraajaat                     	|
| Notebook<br>Notebooks                                                       	| دفتر<br>دفاتر                       	| duftar<br>dafaater                         	|
| Watch, clock<br>Watches, clocks                                             	| ساعة<br>ساعات                       	| saa'a<br>saa'aat                           	|
| Car<br>Cars                                                                 	| سيارة<br>سيارات                     	| sayara<br>sayaaraat                        	|
| At                                                                          	| عند                                 	| ind                                        	|
| Pen, pencil<br>Pen, pencils                                                 	| قلم<br>أقلام                        	| qalam<br>aqlaam                            	|
| Book<br>Books                                                               	| كتاب<br>كتب                         	| kitaab<br>kutub                            	|
| Town, city<br>Towns, cities                                                 	| مدينة<br>مدن                        	| madinat<br>mudn                            	|
| Tape recorder<br>Tape recorders                                             	| مسجّلة<br>مسجّلات                     	| musajjala<br>musajjalaat                   	|
| Key<br>Keys                                                                 	| مفتاح<br>مفاتيح                     	| miftaah<br>mafaateeh                       	|
| Eyeglasses<br>Eyeglasses (pl)                                               	| نظّارة<br>نظّارات                     	| nazzara<br>nazzaraat                       	|
| Telephone<br>Telephones                                                     	| هاتف<br>هواتف                       	| hatif<br>hawatif                           	|
| State<br>States                                                             	| ولاية<br>ولايات                     	| wilaya<br>wilayat                          	|