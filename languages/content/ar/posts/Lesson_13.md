+++
title = "Lesson 13"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-20"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|    English    | Arabic | Transliteration |
|:-------------:|:------:|:---------------:|
|   Sometimes   |        |                 |
|    To give    |        |                 |
|     Which     |        |                 |
| If you please |        |                 |
|    Usually    |        |                 |
|     To do     |        |                 |
|    To read    |        |                 |
|    To write   |        |                 |
|    Magazine   |        |                 |
|     Please    |        |                 |