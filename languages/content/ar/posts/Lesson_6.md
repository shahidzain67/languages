+++
title = "Lesson 6"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-06"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English                               	| Arabic          	| Transliteration      	|
|---------------------------------------	|-----------------	|----------------------	|
| Door<br>Doors                         	| باب<br>أبواب    	| baab<br>abwaab       	|
| Calculator<br>Calculators             	| حاسبة<br>حاسبات 	| haasiba<br>haasibaat 	|
| Bag<br>Bags                           	| حقيبة<br>حقائب  	| haqeeba<br>haqaa'ib  	|
| Picture<br>Pictures                   	| صورة<br>صور     	| sura<br>suwr         	|
| Table<br>Tables                       	| طاولة<br>طاولات 	| taawela<br>taawelaat 	|
| Chair<br>Chairs                       	| كرسيّ<br>كراسٍ    	| kursee<br>karaasin   	|
| Blackboard<br>Blackboards             	| لوح<br>الواح    	| lawhu<br>alwaah      	|
| Pencil Sharpener<br>Pencil Sharpeners 	| مبراة<br>مبارٍ   	| mibraa<br>mabaarin   	|
| Town, city<br>Towns, cities           	| مدينة<br>مدن    	| madina<br>mudn       	|
| Ruler<br>Rulers                       	| مسطرة<br>مساطر  	| mistara<br>masaatir  	|
| Eraser<br>Erasers                     	| ممحاة<br>مماحٍ   	| mimhaa<br>mamaahin   	|
| Window<br>Windows                     	| نافذة<br>نوافذ  	| naafida<br>nawaafida 	|
| This (m)<br>This (f)                  	| هذا<br>هذه      	| haadaa<br>hadehee    	|
| Sheet of paper <br>Sheets of paper    	| ورقة<br>ورقات   	| waraqa<br>waraaqaat  	|
| State<br>States                       	| ولاية<br>ولايات 	| waaleya<br>waaleyaat 	|