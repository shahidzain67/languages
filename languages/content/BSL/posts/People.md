+++
title = "People"
description = ""
tags = [
    "BSL"
]
date = "2021-11-09"
categories = [
    "Week 5"
]
menu = "main"
+++

#### Summary
<video width="50%" height="50%" controls loop>
    <source src="/Mya_5.mp4" type="video/mp4">
</video>

## Hair
<video controls loop>
    <source src="/hair.mp4" type="video/mp4">
</video>

## Curly
<video controls loop>
    <source src="/curly.mp4" type="video/mp4">
</video>

## Straight
<video controls loop>
    <source src="/straight.mp4" type="video/mp4">
</video>

## Eyes
<video controls loop>
    <source src="/eye.mp4" type="video/mp4">
</video>

## Tall
<video controls loop>
    <source src="/tall.mp4" type="video/mp4">
</video>

## Short
<video controls loop>
    <source src="/short.mp4" type="video/mp4">
</video>

## Glasses
<video controls loop>
    <source src="/glasses.mp4" type="video/mp4">
</video>

## Woman/Girl
<video controls loop>
    <source src="/woman.mp4" type="video/mp4">
</video>

## Boy
<video controls loop>
    <source src="/boy.mp4" type="video/mp4">
</video>

## Man
<video controls loop>
    <source src="/man.mp4" type="video/mp4">
</video>

## Family
<video controls loop>
    <source src="/family.mp4" type="video/mp4">
</video>

## Daughter
<video controls loop>
    <source src="/daughter.mp4" type="video/mp4">
</video>

## Son
<video controls loop>
    <source src="/son.mp4" type="video/mp4">
</video>

## Girlfriend
<video controls loop>
    <source src="/girlfriend.mp4" type="video/mp4">
</video>

## Boyfriend
<video controls loop>
    <source src="/boyfriend.mp4" type="video/mp4">
</video>

## Wife
<video controls loop>
    <source src="/wife.mp4" type="video/mp4">
</video>

## Husband
<video controls loop>
    <source src="/husband.mp4" type="video/mp4">
</video>

## Dad
<video controls loop>
    <source src="/dad.mp4" type="video/mp4">
</video>

## Mum
<video controls loop>
    <source src="/mum.mp4" type="video/mp4">
</video>

## Grandmother
<video controls loop>
    <source src="/grandmother.mp4" type="video/mp4">
</video>

## grandfather
<video controls loop>
    <source src="/grandfather.mp4" type="video/mp4">
</video>

## Cousin
<video controls loop>
    <source src="/cousin.mp4" type="video/mp4">
</video>

## Aunt
<video controls loop>
    <source src="/aunt.mp4" type="video/mp4">
</video>

## Uncle
<video controls loop>
    <source src="/uncle.mp4" type="video/mp4">
</video>

## Brother
<video controls loop>
    <source src="/brother.mp4" type="video/mp4">
</video>

## Sister
<video controls loop>
    <source src="/sister.mp4" type="video/mp4">
</video>

## Clothes
<video controls loop>
    <source src="/clothes.mp4" type="video/mp4">
</video>

## Jumper
<video controls loop>
    <source src="/jumper.mp4" type="video/mp4">
</video>

## Dress
<video controls loop>
    <source src="/Dress.mp4" type="video/mp4">
</video>

## Shirt
<video controls loop>
    <source src="/shirts.mp4" type="video/mp4">
</video>

## Skirt
<video controls loop>
    <source src="/Skirt.mp4" type="video/mp4">
</video>

## T-shirt
<video controls loop>
    <source src="/t-shirt.mp4" type="video/mp4">
</video>

## Tie
<video controls loop>
    <source src="/tie.mp4" type="video/mp4">
</video>

## Bowtie
<video controls loop>
    <source src="/bow-tie.mp4" type="video/mp4">
</video>

## Jeans
<video controls loop>
    <source src="/Jeans.mp4" type="video/mp4">
</video>

## Hat
<video controls loop>
    <source src="/hat.mp4" type="video/mp4">
</video>

## Socks
<video controls loop>
    <source src="/sock.mp4" type="video/mp4">
</video>

## Shoes
<video controls loop>
    <source src="/shoes.mp4" type="video/mp4">
</video>

## Shorts
<video controls loop>
    <source src="/shorts.mp4" type="video/mp4">
</video>

## Sandals
<video controls loop>
    <source src="/sandals.mp4" type="video/mp4">
</video>

## Boots
<video controls loop>
    <source src="/boots.mp4" type="video/mp4">
</video>

## Freckles
<video controls loop>
    <source src="/freckles.mp4" type="video/mp4">
</video>

## Piercings
<video controls loop>
    <source src="/piercing.mp4" type="video/mp4">
</video>

## Same
<video controls loop>
    <source src="/same.mp4" type="video/mp4">
</video>