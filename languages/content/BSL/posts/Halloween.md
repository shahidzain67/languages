+++
title = "Halloween"
description = ""
tags = [
    "BSL"
]
date = "2021-10-26"
categories = [
    "Week 3"
]
menu = "main"
+++

#### Summary
<video width="50%" height="50%" controls loop>
    <source src="/Mya_3.mp4" type="video/mp4">
</video>

## Halloween
<video controls loop>
    <source src="/Halloween.mp4" type="video/mp4">
</video>

## Poison/Medicine
<video controls loop>
    <source src="/medicine.mp4" type="video/mp4">
</video>

## Witch
<video controls loop>
    <source src="/witch.mp4" type="video/mp4">
</video>

## Broom
<video controls loop>
    <source src="/broom.mp4" type="video/mp4">
</video>

## Magic
<video controls loop>
    <source src="/magic.mp4" type="video/mp4">
</video>

## Pumpkin
<video controls loop>
    <source src="/pumpkin.mp4" type="video/mp4">
</video>

## Children
<video controls loop>
    <source src="/children.mp4" type="video/mp4">
</video>

## Clown
<video controls loop>
    <source src="/clown.mp4" type="video/mp4">
</video>

## Laugh
<video controls loop>
    <source src="/laugh.mp4" type="video/mp4">
</video>

## Sweet
<video controls loop>
    <source src="/sweet.mp4" type="video/mp4">
</video>

## Trick
<video controls loop>
    <source src="/trick.mp4" type="video/mp4">
</video>

## Bat
<video controls loop>
    <source src="/bat.mp4" type="video/mp4">
</video>

## Scary/scared
<video controls loop>
    <source src="/scared.mp4" type="video/mp4">
</video>

## Haunted house
<video controls loop>
    <source src="/haunted_house.mp4" type="video/mp4">
</video>

## Costume
<video controls loop>
    <source src="/costume.mp4" type="video/mp4">
</video>

## Cauldron/Pot
<video controls loop>
    <source src="/pot.mp4" type="video/mp4">
</video>

## Black
<video controls loop>
    <source src="/black.mp4" type="video/mp4">
</video>

## Cat
<video controls loop>
    <source src="/cat.mp4" type="video/mp4">
</video>

## Dark night
<video controls loop>
    <source src="/dark_night.mp4" type="video/mp4">
</video>

## Blood
<video controls loop>
    <source src="/blood.mp4" type="video/mp4">
</video>

## Fake
<video controls loop>
    <source src="/fake.mp4" type="video/mp4">
</video>

## Moon
<video controls loop>
    <source src="/moon.mp4" type="video/mp4">
</video>

## Vampire
<video controls loop>
    <source src="/vampire.mp4" type="video/mp4">
</video>

## Zombie
<video controls loop>
    <source src="/zombie.mp4" type="video/mp4">
</video>

## Skeleton
<video controls loop>
    <source src="/skeleton.mp4" type="video/mp4">
</video>

## Party
<video controls loop>
    <source src="/party.mp4" type="video/mp4">
</video>

## Torch
<video controls loop>
    <source src="/torch.mp4" type="video/mp4">
</video>

## Grave
<video controls loop>
    <source src="/grave.mp4" type="video/mp4">
</video>

## Graveyard
<video controls loop>
    <source src="/graveyard.mp4" type="video/mp4">
</video>

## Dead
<video controls loop>
    <source src="/dead.mp4" type="video/mp4">
</video>

## Frankenstein
<video controls loop>
    <source src="/frankenstein.mp4" type="video/mp4">
</video>