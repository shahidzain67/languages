+++
title = "Lesson 10"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-13"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|                       English                       |      Arabic     |   Transliteration  |
|:---------------------------------------------------:|:---------------:|:------------------:|
|                       Biology                       |      احياء      |       'ahya'       |
|                    But<br>Rather                    |        بل       |         bal        |
|                Building<br>Buildings                | بناية<br>بنايات | binaya<br>binayaat |
|                       To study                      |   درس<br>يدرس   |   daras<br>yadrus  |
|                     Bed<br>Beds                     |   سرير<br>أسرّة  |    sarir<br>asrr   |
| School subject, course<br>Schools subjects, courses |   مادّة<br>موادّ  |   maada<br>mawadd  |
|                         With                        |        مع       |         mae        |