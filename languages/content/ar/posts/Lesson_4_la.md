+++
title = "Lesson 4 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-05"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

|                                   English                                   |             Arabic            |             Transliteration            |
|:---------------------------------------------------------------------------:|:-----------------------------:|:--------------------------------------:|
|                                     What                                    |              أَيْش              |                   ēš                   |
|                                      Or                                     |               ولّ              |                  willa                 |
|                                     Son                                     |              ابن              |               ibn<br>iben              |
|                                   Welcome                                   |           أهلا وسهلا          |              ’ahla u-sahla             |
|                                    Money                                    |             مِصْرِيِّة             |                 maṣari                 |
|                     Smart (m)<br>Smart (f)<br>Smart (pl)                    |    شاطِر<br>شَاطرَة<br>شَاطرِيْن    |        šāṭer<br>šāṭra<br>šāṭrīn        |
|                                   Matches                                   |             كِبْرِيْت             |                 kibrīt                 |
|                                     Time                                    |              وَقِت              |              waqt<br>waqet             |
|                                  The truth                                  |               حَقّ              |                 il-ḥaqq                |
|                                Book<br>Books                                |          كْتَاْب<br>كِتُب          |              ktāb<br>kutob             |
|                              School<br>Schools                              |         مَدْرَسِة<br>مَدَاْرِس        |           madrase<br>madāres           |
|                                  Yesterday                                  |             مْبَاْرِح             |           embāreḥ<br>embēreḥ           |
|                            Notebook<br>Notebooks                            |         دَفتَر<br>دَفَاتِر         |            daftar<br>dafāter           |
|                                     With                                    |               مع              |                  maƈi                  |
|                                     Want                                    |               بدّ              |                  bidd                  |
|              I want<br>You want (m)<br>You want (f)<br>He wants             |    بدّي<br>بدّك<br>بدّك<br>بدّه   |   biddi<br>biddak<br>biddek<br>biddo   |
| I don't want<br>You don't want (m)<br>You don't want (f)<br>He doesn't want | بدّيش<br>بدّكش<br>بدّكيش<br>بدّوش | biddīš<br>biddakš<br>biddkīš<br>biddōš |
|                            For<br>For the sake of                           |         منشان<br>عشان         |             minšān<br>ƈašān            |
|                                     Like                                    |              متل              |              mitl<br>mitel             |
|                               Something small                               |            شيء صغير           |                'iṡi zġīr               |
|                                Anything else                                |            شيء تاني           |                'iṡi tāni               |