+++
title = "Lesson 1"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-29"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English 	| Arabic 	| Transliteration 	|
|:-:	|:-:	|-	|
| Name<br>Names 	| اسم<br>الأسماء 	| Ism<br>Ismaa 	|
| Goodbye 	| الى اللقاء 	| Ilul liqaa' 	|
| I 	| أنا 	| Ana 	|
| Hello (greeting) 	| مرحبا 	| Murh'aban 	|
| Hello (response) 	| اهلاً 	| Ahlan 	|
| Nice to meet you 	| تشرفنا 	| Tashurrufnaa 	|
| Goodbye 	| مع السلامة 	| Má assalaama 	|