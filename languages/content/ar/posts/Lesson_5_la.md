+++
title = "Lesson 5 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-06"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

|              English             |     Arabic     |  Transliteration  |
|:--------------------------------:|:--------------:|:-----------------:|
|              Sister              |       أُخت      |        'uꜧt       |
|         Must<br>Necessary        |      لَازِم      |       lāzem       |
|     Must not<br>Not necessary    |     مش لَازِم    |     muš lāzem     |
|        Afterwards<br>Later       |      بَعْدَيْن     |       baƈdēn      |
|               Don't              |      بلاش      |       balāš       |
| For<br>Because of<br>In order to |      عَشَان      |       ƈašān       |
|       Sick (m)<br>Sick (f)       |  مريض<br>مريضة |  marīḍ<br>marīḍa  |
|              Doctor              |      دكتور     |       doktōr      |
|              Mother              |       ام       |    'imm<br>'umm   |
|          Chair<br>Chairs         |      كرسيّ      |  kursi<br>karāsi  |
|      Newspaper<br>Newspapers     | جريدة<br>جراؤد | jarīde<br>jarāyed |
|              To here             |     لا هون     |       la-hōn      |
|               There              |      هناك      |       hunāk       |
|               Good               |      كويّس      |      kwayyes      |
|             A little             |       شْوَيّ      |       šwayy       |
|          On the contrary         |       مبل      |       mbala       |