+++
title = "Numbers"
description = ""
tags = [
    "Urdu"
]
date = "2020-06-23"
categories = [
    "Ya Basic"
]
menu = "main"
+++

## 1-10

| English   	| Urdu      	|
|-----------	|-----------	|
| 0 	| ۰ 	| (sifar) صفر 	|   	
| 1 	| ۱ 	| (ek) ایک 	    | 	
| 2 	| ۲ 	| (do) دو 	    | 
| 3 	| ۳ 	| (tīn) تین 	| 
| 4 	| ۴ 	| (chār) چار 	| 
| 5 	| ۵ 	| (pāṅch) پانچ 	| 
| 6 	| ۶ 	| (chhaḥ) چھ 	| 
| 7 	| ۷ 	| (sāt) سات 	| 
| 8 	| ۸ 	| (āṭh) آٹھ 	| 
| 9 	| ۹ 	| (nau) نو 	    | 
| 10 	| ۱۰ 	| (das) دس 	    |

## 11-20

| English   	| Urdu      	|
|-----------	|-----------	|
| 11 	        | (gyārah) گیارہ 	    |
| 12 	        | (bārah) بارہ 	        |
| 13 	        | (tērah) تیرہ 	        |
| 14 	        | (chaudah) چودہ 	    |
| 15 	        | (paṃdrah) پندرہ 	    |
| 16 	        | (solah) سولہ 	        |
| 17 	        | (satrah) سترہ 	    |
| 18 	        | (aṭṭhārah) اٹھارہ 	|
| 19 	        | (unnis) انیس 	        |
| 20 	        | (bīs) بیس 	        |

## 21-30

| English   	| Urdu      	|
|-----------	|-----------	|
| 21 	        | (ikkīs) اکیس 	        |
| 22 	        | (bāīs) بائیس 	        |
| 23 	        | (tēīs) تےيس 	        |
| 24 	        | (chaubīs) چوبیس 	    |
| 25 	        | (pachchīs) پچیس 	    |
| 26 	        | (chhabbīs) چھببیس 	|
| 27 	        | (sattāis) ستااس 	    |
| 28 	        | (aṭṭhāis) اٹھااس 	    |
| 29 	        | (untīs) انتيس 	    |
| 30 	        | (tīs) تیس 	        |

## 31-40

| English   	| Urdu      	|
|-----------	|-----------	|
| 31 	        | (iktīs) اكتيس 	    |   	
| 32 	        | (battīs) بتیس 	    |   
| 33 	        | (taiṃtīs) تینتیس 	    |   	
| 34 	        | (chauṃtīs) چونتیس 	|   	
| 35 	        | (paiṃtīs) پینتیس 	    |   	
| 36 	        | (chhattīs) چھتیس 	    |      	
| 37 	        | (saiṃtīs) سینتیس 	    |   	
| 38 	        | (aṛtīs) اڑتیس 	    |   	
| 39 	        | (untālīs) انتالیس 	|   	
| 40 	        | (chālīs) چالیس 	    |

## 50-100

| English   	| Urdu      	|
|-----------	|-----------	|
| 50 	        | (pachās) پچاس 	    |   	
| 60 	        | (sāṭh) ساٹھ 	        |   	
| 70 	        | (sattar) ستر 	        |   	
| 80 	        | (assī) اسی 	        |   	
| 90 	        | (nabbē) نوے 	        |   	
| 100 	        | (sau) سو 	            |

## 1000+

| English   	| Urdu      	|
|-----------	|-----------	|
| 1,000         | (hazār) ہزار 	        |   	
| 100,000 	    | (lākh) لاکھ          	 |   	
| 1 million 	| (das lākh) دس لاکھ 	 |   	
| 10 million 	| (karoṛ) کروڑ 	        |  

## Ordinals

| English   	| Urdu      	|
|-----------	|-----------	|
| First     	| (pehela) پہلا          |
| Second        | (deosera) دوسرا 	    |
| Third         | (teysera) تیسرا 	    |
| Fourth        | (cheoteha) چوتھا 	    |
| Fifth         | (panecheoan) پانچواں 	|
| Sixth         | (cheheṭa) چھٹا 	    |
| Seventh       | (sateoan) ساتواں 	    |
| Eighth        | (aṭeheoan) آٹھواں 	|
| Ninth         | (neoan) نواں 	        |