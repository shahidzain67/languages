+++
title = "Verbs"
description = ""
tags = [
    "Spanish"
]
date = "2020-06-23"
categories = [
    "Advanced"
]
menu = "main"
+++


## New Words &emsp;|&emsp; Nuevas Palabras

| English          	| Spanish        	|
|------------------	|----------------	|
| To get used to   	| Acostumbrarse  	|
| To grasp         	| Agarrar        	|
| To resemble  	    | Asemejarse       	|
| To drag      	    | Arrastrar        	|
| To sniff          | Huelar            |
| To prowl          | Rondar            |
| To run away       | Huir              |
| To tie            | Atar              |

## Practice &emsp;|&emsp; Práctica

#### Acostumbrarse | To get used to
<details>
    <summary>Lleva tiempo acostumbrarse al frío</summary>
    It takes time to get used to the cold
</details>

---

#### Agarrar | To grasp  
<details>
    <summary>Ese mono está agarrando el árbol</summary>
    That monkey is grasping the tree
</details>

---

#### Asemejarse | To resemble  
<details>
    <summary>Él le asemeja a mi madre</summary>
    He resembles my mother
</details>

---

#### Arrastrar | To drag  
<details>
    <summary>¿Dónde estás arrastrando esa bolsa de manzanas?</summary>
    Where are you dragging that bag of apples?
</details>

---

#### Huelar | To sniff  
<details>
    <summary>Las criaturas de la luna huelen las manzanas</summary>
    The creatures of the moon sniff the apples
</details>

---

#### Rondar | To prowl  
<details>
    <summary>Él está rodando en la playa</summary>
    He is prowling on the beach
</details>

---

#### Huir | To run away  
<details>
    <summary>Debemos tomar nuestras cosas y huir.</summary>
    We have to take our possessions and flee.
</details>

---

#### Atar | To tie  
<details>
    <summary>Pero si quieres atar mis manos...</summary>
    Now, if you want to tie my hands...
</details>