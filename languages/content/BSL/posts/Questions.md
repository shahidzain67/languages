+++
title = "Questions and Colours"
description = ""
tags = [
    "BSL"
]
date = "2021-11-02"
categories = [
    "Week 4"
]
menu = "main"
+++

#### Summary
<video width="50%" height="50%" controls loop>
    <source src="/Mya_4.mp4" type="video/mp4">
</video>

## Who
<video controls loop>
    <source src="/who.mp4" type="video/mp4">
</video>

## What
<video controls loop>
    <source src="/what.mp4" type="video/mp4">
</video>

## When
<video controls loop>
    <source src="/when.mp4" type="video/mp4">
</video>

## Where
<video controls loop>
    <source src="/where.mp4" type="video/mp4">
</video>

## Why/Because
<video controls loop>
    <source src="/why.mp4" type="video/mp4">
</video>

## Which
<video controls loop>
    <source src="/which.mp4" type="video/mp4">
</video>

## How
<video controls loop>
    <source src="/how.mp4" type="video/mp4">
</video>

## Want
<video controls loop>
    <source src="/want.mp4" type="video/mp4">
</video>

## Arrive
<video controls loop>
    <source src="/arrive.mp4" type="video/mp4">
</video>

## What's your name?
<video controls loop>
    <source src="/nameyouwhat.mp4" type="video/mp4">
</video>

## Toilet
<video controls loop>
    <source src="/toilet.mp4" type="video/mp4">
</video>

## Birthday
<video controls loop>
    <source src="/birthday.mp4" type="video/mp4">
</video>

## Job
<video controls loop>
    <source src="/job.mp4" type="video/mp4">
</video>

## Colour
<video controls loop>
    <source src="/colour.mp4" type="video/mp4">
</video>

## Favourite
<video controls loop>
    <source src="/favourite.mp4" type="video/mp4">
</video>

## Phone
<video controls loop>
    <source src="/phone.mp4" type="video/mp4">
</video>

## Shoes
<video controls loop>
    <source src="/shoes.mp4" type="video/mp4">
</video>

## Live
<video controls loop>
    <source src="/live.mp4" type="video/mp4">
</video>

## Time
<video controls loop>
    <source src="/time.mp4" type="video/mp4">
</video>

## Sign
<video controls loop>
    <source src="/sign.mp4" type="video/mp4">
</video>

## Do
<video controls loop>
    <source src="/do.mp4" type="video/mp4">
</video>

## Wine
<video controls loop>
    <source src="/wine.mp4" type="video/mp4">
</video>

## Weekend
<video controls loop>
    <source src="/weekend.mp4" type="video/mp4">
</video>

## Red
<video controls loop>
    <source src="/red.mp4" type="video/mp4">
</video>

## White
<video controls loop>
    <source src="/white.mp4" type="video/mp4">
</video>

## Blue
<video controls loop>
    <source src="/blue.mp4" type="video/mp4">
</video>

## Green
<video controls loop>
    <source src="/green.mp4" type="video/mp4">
</video>

## Orange
<video controls loop>
    <source src="/orange.mp4" type="video/mp4">
</video>

## Yellow
<video controls loop>
    <source src="/yellow.mp4" type="video/mp4">
</video>

## Brown
<video controls loop>
    <source src="/brown.mp4" type="video/mp4">
</video>

## Black
<video controls loop>
    <source src="/black.mp4" type="video/mp4">
</video>

## Grey
<video controls loop>
    <source src="/grey.mp4" type="video/mp4">
</video>

## Silver
<video controls loop>
    <source src="/silver.mp4" type="video/mp4">
</video>

## Gold
<video controls loop>
    <source src="/gold.mp4" type="video/mp4">
</video>

## Purple
<video controls loop>
    <source src="/purple.mp4" type="video/mp4">
</video>

## Pink
<video controls loop>
    <source src="/pink.mp4" type="video/mp4">
</video>