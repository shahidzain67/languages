+++
title = "Lesson 2"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-30"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English 	| Arabic 	| Transliteration 	|
|:-:	|:-:	|-	|
| I 	|  أنا	| Ana 	|
| You (m) 	| أنت 	| Anta 	|
| You (f) 	|  أنت	| Anti 	|
| He 	|  هو	| Huwa 	|
| She 	|  هي	| Heeya 	|
| No 	| لا | La 	|
| Yes 	|  نعم 	| n'am 	|