+++
title = "Connectors"
description = ""
tags = [
    "Spanish"
]
date = "2020-06-24"
categories = [
    "Advanced"
]
menu = "main"
+++


## New Words &emsp;|&emsp; Nuevas Palabras

| English      	| Spanish          	|
|--------------	|------------------	|
| Consequently 	| Por consiguiente 	|
| Indefinitely 	| Indefinidamente  	|
| Pitifully    	| Lastimosamente   	|
| As far as I know 	| Que yo sepa    	|
| That he knew     	| Que él supiera 	|
| As far as I have 	| Que yo haya    	|


## Practice &emsp;|&emsp; Práctica

#### Por consiguiente | Consequently
<details>
    <summary>En consecuencia, la sopa de plátano era demasiado dulce.</summary>
    Consequently, the banana soup was too sweet
</details>

---

#### Indefinidamente | Indefinitely
<details>
    <summary>Voy a ser presidente indefinidamente</summary>
    I am going to be president indefinitely 
</details>

---

#### Lastimosamente | Pitifully
<details>
    <summary>La gatita maulló lastimosamente</summary>
    The kitten mewled pitifully 
</details>

---

#### Que yo sepa | As far as I know
<details>
    <summary>Que yo sepa, es un niño</summary>
    As far as I know, it's a boy
</details>

--- 

#### Que él supiera | That he knew
<details>
    <summary>Parece que supiera algo que el resto de nosotros no.</summary>
    Looks like he knows something the rest of us don't.
</details>

--- 

#### Que yo haya | As far as I have
<details>
    <summary>Por lo que he visto, los cocodrilos son malos</summary>
    As far as I have seen, crocodiles are bad
</details>