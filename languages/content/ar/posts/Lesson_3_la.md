+++
title = "Lesson 3 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-03"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

| English                         	| Arabic           	| Transliteration                      	|
|:---------------------------------:|:------------------:|:--------------------------------------:|
| This (m)<br>This one<br>That    	| هَيدَا             	| hāda                                 	|
| This (f)<br>That                	| هَاي<br>هَادي      	| hāy<br>hādi                          	|
| These<br>Those                  	| هَدَوْل             	| hadōl                                	|
| This<br>That<br>These <br>Those 	| هَل               	| hal                                  	|
| House<br>House (pl)             	| دَاْر<br>دُوْر       	| dār<br>dūr                           	|
| From<br>Out of                  	| من               	| min                                  	|
| Unemployment                    	| بَطَاْلِة            	| baṭāle                               	|
| Tired<br>Tired out              	| تَعْبَاْن<br>تَعبَانِيْن 	| taƈbān<br>taƈbānīn (pl)              	|
| Busy                            	| مَشْغُوْل<br>مَشغُوْلِيْن 	| mašgūl<br>mašgūlīn (pl)              	|
| Hard<br>Difficult               	| صَعِب              	| ṣaƈb<br>saƈeb                        	|
| Very<br>Much<br>Many            	| كْتِيْر             	| ktīr                                 	|
| Now                             	| هَلَّق              	| hallaq                               	|
| We                              	| اِحنَا             	| 'iḥna                                	|
| Our                             	| نا               	| -na                                  	|
| You (pl)                        	| اِنْتُو             	| 'intu                                	|
| Your                            	| كم<br>كو         	| -kom (Jerusalem)<br>-ku (Galilee)    	|
| They                            	| همّ<br>هنّ         	| humme (Jerusalem)<br>henne (Galilee) 	|
| Their                           	| هم<br>هن         	| -hom<br>-hen                         	|
| Outside                         	| بَرَّا              	| barra                                	|