+++
title = "Lesson 1 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-29"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

| English                        	| Arabic            	| Transliteration      	|
|:-------------------------------:	|:-------------------:	|:----------------------:	|
| Who                            	| مِيْن               	| mīn                  	|
| Where                          	| وين               	| wēn                  	|
| Daughter/Girl<br>Girls            | بِنت<br>بنات            	| bint<br>banāt                 	|
| I                              	| أنا               	| 'ana                 	|
| Yes                            	| آه<br>أَيْوَا<br>نَعَم 	| 'ā<br>'aywa<br>naƈam 	|
| This<br>These<br>That<br>Those 	| هَل                	| hal-                 	|
| This (m)<br>That (m)           	| هَاْد               	| hāda                 	|
| This (f)<br>That (f)           	| هَي<br>هَادِ         	| hāy<br>hādi          	|
| You (m)                        	| انتِا<br>اِنْتَ       	| 'inte<br>'inta       	|
| You (f)                        	| اِنْتِ               	| 'inti                	|
| The                            	| اِل<br>لِ           	| il-<br>l-            	|
| And                            	| ـُ<br>وَ            	| u-<br>w-             	|
| Oh!<br>Hey!                    	| يَا<br>يَا          	| ya<br>yā             	|
| What?                          	| شُوْ<br>شُـ          	| ŝū<br>ŝu             	|
| Here                           	| هَوْن               	| hōn                  	|
| House<br>Houses                   | بَيْت<br> بيوت          | bēt<br>byūt            	|
| Not (m)<br>Not (f)             	| مِش<br>مُش          	| miš<br>muš           	|
| No                             	| لا                	| la'                  	|
| Beautiful                      	| حِلْو               	| ḥilu                 	|
| Big                            	| كْبِيْر              	| kbīr                 	|
| Lives (m)<br>Living (m)        	| سَاكِن              	| sāken<br>sākīn (pl)          	|
| Lives (f)<br>Living (f)        	| سَاكْنِة             	| sākne                	|
| Also<br>Too<br>As well<br>Else 	| كَمَاْن              	| kamān                	|
| Manager<br>Boss                	| مُدِيْر              	| mudīr                	|
| In<br>Inside<br>At             	| فِي                	| fi                   	|
| Ok                                | طيب                  | ṭayyeb                |
| That's enough                     | بكفّي                  | bikaffi               |