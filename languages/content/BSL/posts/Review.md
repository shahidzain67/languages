+++
title = "Review/Conversation Practice"
description = ""
tags = [
    "BSL"
]
date = "2021-11-23"
categories = [
    "Week 6"
]
menu = "main"
+++

# New vocab

#### Yesterday
<video width="50%" height="50%" controls loop>
    <source src="/yesterday.mp4" type="video/mp4">
</video>

#### Today
<video width="50%" height="50%" controls loop>
    <source src="/today.mp4" type="video/mp4">
</video>

#### Tomorrow
<video width="50%" height="50%" controls loop>
    <source src="/tomorrow.mp4" type="video/mp4">
</video>

#### Before
<video width="50%" height="50%" controls loop>
    <source src="/before.mp4" type="video/mp4">
</video>

#### After
<video width="50%" height="50%" controls loop>
    <source src="/after.mp4" type="video/mp4">
</video>

#### Next 
<video width="50%" height="50%" controls loop>
    <source src="/next.mp4" type="video/mp4">
</video>

#### Last (previous)
<video width="50%" height="50%" controls loop>
    <source src="/last.mp4" type="video/mp4">
</video>

# Conversation

#### Hi
<video width="50%" height="50%" controls loop>
    <source src="/hi.mp4" type="video/mp4">
</video>

#### Good Morning
<video width="50%" height="50%" controls loop>
    <source src="/good_morning.mp4" type="video/mp4">
</video>

#### Good Afternoon
<video width="50%" height="50%" controls loop>
    <source src="/good-afternoon.mp4" type="video/mp4">
</video>

#### Good Evening
<video width="50%" height="50%" controls loop>
    <source src="/good-evening.mp4" type="video/mp4">
</video>

#### Good Night
<video width="50%" height="50%" controls loop>
    <source src="/goodnight.mp4" type="video/mp4">
</video>

#### How are you?
<video width="50%" height="50%" controls loop>
    <source src="/how_are_you.mp4" type="video/mp4">
</video>

#### What's your name?
<video width="50%" height="50%" controls loop>
    <source src="/nameyouwhat.mp4" type="video/mp4">
</video>

#### Where are you from?
<video width="50%" height="50%" controls loop>
    <source src="/fromyouwhere.mp4" type="video/mp4">
</video>

#### How old are you?
<video width="50%" height="50%" controls loop>
    <source src="/how_old_are_you.mp4" type="video/mp4">
</video>

#### Who is that?
<video width="50%" height="50%" controls loop>
    <source src="/who_that1.mp4" type="video/mp4">
</video>
<video width="50%" height="50%" controls loop>
    <source src="/who_that2.mp4" type="video/mp4">
</video>

#### What's your favourite colour?
<video width="50%" height="50%" controls loop>
    <source src="/favourite_colour.mp4" type="video/mp4">
</video>

#### What is your date of birth?
<video width="50%" height="50%" controls loop>
    <source src="/dob.mp4" type="video/mp4">
</video>

#### What do you do (for work)?
<video width="50%" height="50%" controls loop>
    <source src="/workwhat.mp4" type="video/mp4">
</video>

#### What's your phone number?
<video width="50%" height="50%" controls loop>
    <source src="/phonenumber.mp4" type="video/mp4">
</video>

#### Where are your shoes from?
<video width="50%" height="50%" controls loop>
    <source src="/shoesfromwhere.mp4" type="video/mp4">
</video>

#### Do you want a drink?
<video width="50%" height="50%" controls loop>
    <source src="/drinkwantyou.mp4" type="video/mp4">
</video>

#### What drink do you want?
<video width="50%" height="50%" controls loop>
    <source src="/drinkwhat.mp4" type="video/mp4">
</video>

#### What do you want to eat?
<video width="50%" height="50%" controls loop>
    <source src="/foodwhich.mp4" type="video/mp4">
</video>

#### Where do you live?
<video width="50%" height="50%" controls loop>
    <source src="/livewhere.mp4" type="video/mp4">
</video>

#### What time is it?
<video width="50%" height="50%" controls loop>
    <source src="/what-time.mp4" type="video/mp4">
</video>

#### How do you sign "cat"?
<video width="50%" height="50%" controls loop>
    <source src="/signcathow.mp4" type="video/mp4">
</video>