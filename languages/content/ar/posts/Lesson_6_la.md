+++
title = "Lesson 6 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-07"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

| English                   | Arabic        | Transliteration   |
|---------------------------|---------------|-------------------|
| Jerusalem                 | القدس         | il-quds           |
| Nazareth                  | الناصرة       | in-nāṣre          |
| Paternal Uncle            |               | ƈamm              |
| Better                    | أحسن          | 'aḥsan            |
| Before<br>Before (+ verb) | قبل<br>قبل ما | qabel<br>qabel-ma |
| Because                   | لانّ           | li'anno           |
| So that                   | حتّ<br>عشان    | ḥatta<br>ƈašān    |
| Old man                   | ختيار         | ꜧetyār            |
| When                      | متى           | 'ēmta             |
| Tomorrow                  | بِكْرَا          | bukra             |
| After                     | بعد           | baƈd              |
| Always                    | دايما         | dāyman            |
| For a long time           | من زمان       | min zamān         |
| How old is he?            | قدّيش عمره     | qaddēš ƈumro?     |
| Not a little              | مش قليل       | miš qalīl         |
| Quickly                   | قوام          | qawām             |
| Bus stop                  | محطّ           | maḥaṭṭa           |
| Bus                       | باص           | bāṣ               |
| Soon                      | بعد الشويّ     | baƈd ešwayy       |
| 9 o'clock                 | الساعه تسع    | is-sēƈa tisƈa     |
| Half an hour              | نصّ ساعة       | nuṣṣ sēƈa         |
| More!                     | كمان          | kamān             |
| Another one               | كمان واحد     | kamān wāḥad       |
| Again                     | كمان مرّا      | kamān marra       |