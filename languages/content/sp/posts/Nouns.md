+++
title = "Nouns"
description = ""
tags = [
    "Spanish"
]
date = "2020-06-29"
categories = [
    "Advanced"
]
menu = "main"
+++

## New Words &emsp;|&emsp; Nuevas Palabras

| English      	| Spanish        	|
|--------------	|----------------	|
| Serenity     	| Serenidad      	|
| Embroidery   	| Bordado        	|
| Tenderness   	| Ternura        	|
| Distributers 	| Distribuidores 	|
| Thicket       | Maraña            |
| Claw/Paw      | Zarpa             |
| Downpour      | Aguacero          |

## Practice &emsp;|&emsp; Práctica

#### Serenidad | Serenity
<details>
    <summary>La serenidad en la caricia del viento.</summary>
    The serenity in the caress of the wind
</details>

---

#### Bordado | Embroidery
<details>
    <summary>Un manto bordado de pensamientos</summary>
    An embroidered cloak of thoughts
</details>

---

#### Ternura | Tenderness
<details>
    <summary>En mi frente la ternura de un beso</summary>
    On my forehead the tenderness of a kiss
</details>

---

#### Distribuidores | Distributers
<details>
    <summary>Mi negocio de patos tiene muchos distribuidores</summary>
    My duck business has many distributors
</details>

---

#### Maraña | Thicket
<details>
    <summary>De la maraña de alianzas dos lados surgen.</summary>
    From the tangle of alliances two sides emerge.
</details>

---

#### Zarpa | Claw/Paw
<details>
    <summary>No parece ser una pezuña o una zarpa.</summary>
     It doesn't really look like a hoof or paw.
</details>

---

#### Aguacero | Downpour
<details>
    <summary>A su hijo le encanta jugar en el aguacero.</summary>
     Their son loves to play in the downpour.
</details>
