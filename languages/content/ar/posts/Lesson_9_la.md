+++
title = "Lesson 9 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-13"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

|            English            |     Arabic     |     Transliteration    |
|:-----------------------------:|:--------------:|:----------------------:|
|       Anyone<br>Anybody       |   حَدَا<br>حَدًا   |      ḥada<br>ḥadd      |
|        Nobody<br>No one       |     ما حداش    | ma-ḥadā-š<br>ma-ḥadd-š |
|        Subject<br>Topic       |      مَوْضُوْع     |         mawḍūƈ         |
|       Minute<br>Minutes       | دْقِيقة<br>دَقَايِق |    daqīqa<br>daqāyeq   |
|           The world           |      عَالَم      |        il-ƈālam        |
|         Coffee<br>Cafe        |      قهوة      |          qahwe         |
|              War              |       حرب      |          ḥarb          |
|             Peace             |      سلام      |          sālam         |
| Mind<br>Sense<br>Intelligence |       عقل      |          ƈaql          |
|             Egypt             |       مصر      |          maṣer         |
|            Egyptian           |      مصري      |          maṣri         |
|            Perhaps            |      يمكن      |         yimken         |
|             People            |       شَعِب      |          šaƈeb         |
|              Beer             |      بيرة      |          bīra          |
|              Cake             |       كيك      |          kaƈek         |
|             Anyway            |    في كل حال   |      ala kull ḥāl      |