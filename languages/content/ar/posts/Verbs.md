+++
title = "Verbs"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-23"
categories = [
    "Ya Basic"
]
menu = "main"
+++


## New Words &emsp;|&emsp; كلمات جديدة

| English  	| Arabic 	|
|----------	|--------	|
| Do    	| فعل    	|
| Work  	| عمل    	|
| Bring 	| أحضر   	|
| Take  	| أخذ    	|
| Put   	| وضع    	|
| Become 	| أصبح   	|
| Change 	| غيّر    	 |
| Use     	| استعمال 	|
| Go      	| اذهب    	|
| Come    	| تأتي    	|
| Laugh   	| يضحك    	|


## Practice &emsp;|&emsp; ممارسة

#### فعل | Do
<details>
    <summary>عندي الكثير من العمل به اليوم</summary>
    I have a lot of work to do today
</details>

---   

#### عمل | Work
<details>
    <summary>هل تعمل أم تدرس</summary>
    Do you work or study? 
</details>

---

#### أحضر | Bring
<details>
    <summary>أحضرت صلصة لحم الضأن إلى الحفلة</summary>
    I brought my lamb sauce to the party
</details>

---   

#### أخذ | Take
<details>
    <summary>شاكا زولو يأخذ سكرين في الشاي</summary>
    Shaka Zulu takes two sugars in his tea
</details>

---   

#### وضع | Put
<details>
    <summary>أضع البصل في حقيبتك</summary>
    I put onions in your bag
</details>

--- 

#### أصبح | Become
<details>
    <summary>سأصبح أستاذ</summary>
    I will become a professor
</details>

--- 

#### غيّر | Change
<details>
    <summary>أريد تغيير قميصي</summary>
    I want to change my shirt
</details>

---

#### استعمال | Use
<details>
    <summary>قالت بأنّها كانت تُريد استعمال الهاتف لدقيقةٍ</summary>
    Said she just wanted to use the phone for a minute.
</details>

---

#### اذهب | Go
<details>
    <summary>هل يمكنك شراء هذا لي؟</summary>
    Can you buy me that?
</details>

---

#### تأتي | Come
<details>
    <summary>تعال إلى حفلتي</summary>
    Come to my party
</details>

---

#### يضحك | Laugh
<details>
    <summary>كنت أضحك كثيرا</summary>
    I used to laugh a lot
</details>