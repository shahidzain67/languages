+++
title = "Days of the Week"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-29"
categories = [
    "Ya Basic"
]
menu = "main"
+++

## New Words &emsp;|&emsp; كلمات جديدة

| English   	| Arabic 	|
|-----------	|--------	|
| Days    	|   أيام	|
| Week      |   أسبوع 	|
| Monday    |   الإثنين   |
| Tuesday   |   الثلاثاء   |
| Wednesday |   الأربعاء   |
| Thursday  |   الخميس   |   
| Friday    |   يوم الجمعة   |
| Saturday  |   يوم السبت   |
| Sunday|   |   الأحد   |


## Practice &emsp;|&emsp; ممارسة

#### أيام | Days
<details>
    <summary>شو اليوم؟</summary>
    What day is it today
</details>

---   

#### أسبوع | Week
<details>
    <summary>يبدأ كل أسبوع يوم الاثنين</summary>
    Every week starts with monday
</details>

--- 
  
#### الإثنين | Monday
<details>
    <summary>اليوم الاثنين</summary>
    Today is Monday
</details>

--- 

#### الثلاثاء | Tuesday
<details>
    <summary>غدا الثلاثاء</summary>
    Tomorrow will be Tuesday
</details>

--- 

#### الأربعاء | Wednesday
<details>
    <summary>الثلاثاء البارحة</summary>
    Yesterday was Wednesday
</details>

--- 

#### الخميس | Thursday
<details>
    <summary>أذهب إلى المدرسة كل يوم خميس</summary>
    I go to school every Thursday
</details>

--- 

#### يوم الجمعة | Friday
<details>
    <summary>يوم الجمعة هو يوم قصير</summary>
    Friday is a short day
</details>

--- 

#### يوم السبت | Saturday
<details>
    <summary>كل يوم سبت أكل بيض</summary>
    Every saturday I eat eggs
</details>

--- 

#### الأحد | Sunday
<details>
    <summary>الأحد هو أسوأ يوم</summary>
    Sunday is the worst day
</details>

--- 