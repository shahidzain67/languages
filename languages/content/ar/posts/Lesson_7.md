+++
title = "Lesson 7"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-07"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English                       	| Arabic          	| Transliteration   	|
|-------------------------------	|-----------------	|-------------------	|
| Beside                        	| بجانب           	| bijaanib          	|
| On                            	| على             	| a'laa             	|
| In                            	| في              	| fee               	|
| Classroom                     	| غرفة صفّ         	| ghurfat sf        	|
| Professor                     	| استاذ           	| 'ustadh           	|
| Too<br>Also                   	| ايضاً            	| aydaan            	|
| On<br>In<br>By<br>With<br>For 	| ب               	| be                	|
| University<br>Universities    	| جامعة<br>جامعات 	| jamiea<br>jamieat 	|
| Side                          	| جانب            	| janib             	|
| Wall<br>Walls                 	| جدار<br>جدران   	| jadar<br>judran   	|
| Aleppo                        	| حلب             	| halab             	|
| Mathematics<br>Calculus       	| رياضيّات         	| ryadyat           	|
| Class<br>Classes              	| صفّ<br>صفوف      	| saf<br>sufuf      	|
| Student (m)<br>Students (m)   	| طالب<br>طلّاب    	| talib<br>tullub   	|
| Student (f)<br>Students (f)   	| طالبة<br>طالبات 	| taliba<br>talibat 	|
| But                           	| لكن<br>لكنّ      	| lakin<br>lakinna  	|
| Not                           	| ليس             	| laysa             	|
| There<br>There is/are         	| هناك            	| hnak              	|
| And                           	| و               	| wa                	|


# Negation of Nominal Sentences

| English                       	| Arabic  	| Transliteration 	|
|-------------------------------	|---------	|-----------------	|
| I don't                       	| لستُ     	| lastu           	|
| We don't                      	| لسنا    	| lasnaa          	|
| You (m) don't                 	| لستَ     	| lasta           	|
| You (f) don't                 	| لستِ     	| lasti           	|
| You (d) don't                 	| لستما   	| lastumaa        	|
| You (m, pl) don't             	| لستم    	| lastum          	|
| You (f, pl) don't             	| لستنّ    	| lastunna        	|
| He doesn't                    	| ليس     	| laysa           	|
| She doesn't                   	| ليست    	| laysat          	|
| They (m, d) don't             	| ليسا    	| laysaa          	|
| They (f, d) don't             	| ليستا   	| laysataa        	|
| They (m) don't                	| ليسوا   	| layswaa         	|
| They (f) don't                	| لسن     	| lasna           	|