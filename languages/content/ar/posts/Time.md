+++
title = "Time"
description = ""
tags = [
    "Arabic"
]
date = "2020-06-24"
categories = [
    "Ya Basic"
]
menu = "main"
+++

## New Words &emsp;|&emsp; كلمات جديدة

| English   	| Arabic 	|
|-----------	|--------	|
| Second    	| ثانية  	|
| Hour      	| ساعة   	|
| Minute    	| دقيقة  	|


## Practice &emsp;|&emsp; ممارسة

#### ثانية | Second
<details>
    <summary>كم ثانية متبقية؟</summary>
    How many seconds are left?
</details>

---   

#### ساعة | Hour
<details>
    <summary>أحتاج إلى ساعة أخرى</summary>
    I need one more hour
</details>

---   

#### ساعة | Hour
<details>
    <summary>بعد مرور عشرون دقيقة</summary>
    Twenty minutes later
</details>