+++
title = "Lesson 2 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-01"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

| English                                                	| Arabic               	| Transliteration          	|
|:--------------------------------------------------------:	|:----------------------:|:--------------------------:|
| How                                                    	| كيف                  	| kīf                      	|
| at<br>by<br>at the home of                             	| عند                  	| ƈindi                    	|
| you (m) have<br>you've got<br>at your house<br>at home 	| في                   	| fī                       	|
| There isn't<br>There aren't                            	| ما فيش               	| ma fī-š                  	|
| Every<br>Each<br>All                                   	| كل                   	| kull                     	|
| Situation<br>State                                     	| حال                  	| ḥāl                      	|
| Right<br>True                                          	| مظبوط                	| maẓbūṭ                   	|
| Thing<br>Something                                     	| شَيْء                  	| 'iši                     	|
| Only<br>But                                            	| بس                   	| bass                     	|
| Boy<br>Children                                        	| وَلَد<br>أَوْلَاْد         	| walad<br>ulād            	|
| That's to say                                          	| يعني                 	| yaƈni                    	|
| Work<br>Job                                            	| شغول                 	| šuġel<br>šuġol           	|
| Pleased<br>Pleased (pl)                                	| مبسوط<br>مبسوطين     	| mabsūṭ<br>mabsūṭīn       	|
| Neighbour<br>Neighbours                                	| الجار<br>الجيران     	| jār<br>jirān             	|
| Money                                                  	| مصاري                	| maṣāri                   	|
| Good morning                                           	| صباح الخير           	| ṣabāḥ il-ꜧēr             	|
| How are you? (m)<br>How are you (f)                    	| كيف حالك<br>كيف حالك 	| kīf ḥālak?<br>kīf ḥālek? 	|
| Fine thank you                                         	| الله يسلمك           	| 'alla isallmak           	|
| These days                                             	| الأيام               	| il-yōm                   	|
| Thus<br>Like that                                      	| هَيْك                  	| hēk                      	|
| Good<br>Good (pl)                                      	| مْنِيْح<br>مْنَاْح         	| mnīḥ<br>mnāḥ             	|
| World                                                  	| دُنيَا                 	| dinya                    	|
| Lesson<br>Lessons                                      	| دَرْس<br>دْرُوْس          	| dars<br>drūs             	|
| There is<br>There are                                  	| في                   	| fī                       	|
| I have got                                             	| عندي                 	| ƈindī                    	|
| Radio                                                  	| راديو                	| rādyo                    	|

