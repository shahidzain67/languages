+++
title = "Time"
description = ""
tags = [
    "Urdu"
]
date = "2020-06-24"
categories = [
    "Ya Basic"
]
menu = "main"
+++

## New Words &emsp;|&emsp; نئے الفاظ

| English  	| Urdu   	|
|----------	|--------	|
| Calendar 	| کیلنڈر 	|
| Year     	| سال    	|
| Week     	| ہفتہ   	|
| Second   	| سیکنڈ  	|
| Hour     	| گھنٹا  	|
| Every Day 	| ہر روز    	|

## Practice &emsp;|&emsp; مشق

#### کیلنڈر | Calendar
<details>
    <summary>کیلنڈر غلط ہے</summary>
    The calendar is wrong
</details>

---

#### سال | Year
<details>
    <summary>یہ گائے کا سال ہے</summary>
    This is the year of the cow
</details>

--- 

#### ہفتہ | Week
<details>
    <summary>پینکیک دن تک دو ہفتے باقی ہیں</summary>
    There's two weeks left until pancake day
</details>

--- 

#### سیکنڈ | Second
<details>
    <summary>کتنے سیکنڈ باقی ہیں؟</summary>
    How many seconds are left?
</details>

--- 

#### گھنٹا | Hour
<details>
    <summary>گھنٹے کے بعد ، یہ کیا گیا تھا</summary>
    Hours later, it was done 
</details>

--- 

#### ہر روز  | Every Day
<details>
    <summary>ہر دن میں بریانی پکاتا ہوں</summary>
    Every day I cook biryani
</details>