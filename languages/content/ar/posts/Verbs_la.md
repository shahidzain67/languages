+++
title = "Verbs - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-07"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

### Subjunctive connectors

1. biddi
    * biddi trūḥ: I want you to go

2. lāzem
    * lāzem etrūḥ: You must go

3. 'aḥsan
    * aḥsan etrūḥ: You'd better go

4. qabel-ma
    * qabel-ma etrūḥ: Before you go

5. ḥatta
    * ḥatta trūḥ: So that you go

6. ƈala-šan 
    * ƈala-šan etrūḥ: So that you go 

# Present/Future

|          |  To go | To bring | To tell | To see | To want | To visit |  To be | To die | To come/go in | To move |
|:--------:|:------:|:--------:|:-------:|:------:|:-------:|:--------:|:------:|:------:|:-------------:|:-------:|
|     I    |  barūḥ |   bajīb  |  baqūl  |  bašūf |  biddi  |   bazūr  |  bakūn |  bamūt |     bafūt     |  bazīḥ  |
|    We    |  bnrūḥ |   bnjīb  |  bnqūl  |  bnšūf |  bidnaa |   bnzūr  |  bnkūn |  bnmūt |     bnfūt     |  bnzīḥ  |
|  You (m) |  btrūḥ |   btjīb  |  btqūl  |  btšūf |  biddak |   btzūr  |  btkūn |  btmūt |     btfūt     |  btzīḥ  |
|  You (f) | btrūḥi |  btjībi  |  btqūli | btšūfi |  biddek |  btzūri  | btkūni | btmūti |     btfūti    |  btzīḥi |
| You (pl) | btrūḥu |  btjību  |  btqūlu | btšūfu | biddkom |  btzūru  | btkūnu | btmūtu |     btfūtu    |  btzīḥu |
|    He    |  birūḥ |   bijīb  |  biqūl  |  bišūf |  biddo  |   bizūr  |  bikūn |  bimūt |     bifūt     |  bizīḥ  |
|    She   |  btrūḥ |   btjīb  |  btqūl  |  btšūf |  biddha |   btzūr  |  btkūn |  btmūt |     btfūt     |  btzīḥ  |
|   They   | birūḥu |  bijību  |  biqūlu | bišūfu | biddhom |  bizūru  | bikūnu | bimūtu |     bifūtu    |  bizīḥu |


# Subjunctive

|               |  To go  | To bring | To tell |  To see | To visit |  To be  |  To die | To come/go in | To move |
|:-------------:|:-------:|:--------:|:-------:|:-------:|:--------:|:-------:|:-------:|:-------------:|:-------:|
|     That I    |   arūḥ  |   ajīb   |   aqūl  |   ašūf  |   azūr   |   akūn  |   amūt  |      afūt     |   azīḥ  |
|    That we    |   nrūḥ  |   njīb   |   nqūl  |   nšūf  |   nzūr   |   nkūn  |   nmūt  |      nfūt     |   nzīḥ  |
|  That you (m) |   trūḥ  |   tjīb   |   tqūl  |   tšūf  |   tzūr   |   tkūn  |   tmūt  |      tfūt     |   tzīḥ  |
|  That you (f) |  trūḥi  |   tjībi  |  tqūli  |   tšūf  |   tzūri  |  tkūni  |  tmūti  |     tfūti     |  tzīḥi  |
| That you (pl) |  trūḥu  |   tjību  |  tqūlu  |  tšūfu  |   tzūru  |  tkūnu  |  tmūtu  |     tfūtu     |  tzīḥu  |
|    That he    |   irūḥ  |   ijīb   |   iqūl  |   išūf  |   izūr   |   ikūn  |   imūt  |      ifūt     |   izīḥ  |
|    That she   |   trūḥ  |   tjīb   |   tqūl  |   tšūf  |   tzūr   |   tkūn  |   tmūt  |      tfūt     |   tzīḥ  |
|   That they   |  irūḥu  |   ijību  |  iqūlu  |  išūfu  |   izūru  |  ikūnu  |  imūtu  |     ifūtu     |  izīḥu  |
| ------------- | ------- |  ------- | ------- | ------- |  ------- | ------- | ------- |    -------    | ------- |
|    Command!   |   rūḥ   |    jīb   |   qūl   |   šūf   |    zūr   |   kūn   |   mūt   |      fūt      |   zīḥ   |


# Past Tense 

|          | To go | To bring | To tell | To see | To visit | To be | To die | To come/go in | To move | To write | To pass by | To leave | To kill |  To ask | To keep quiet | To request | To take |  To hit |  To pay | To enter |
|:--------:|:-----:|:--------:|:-------:|:------:|:--------:|:-----:|:------:|:-------------:|:-------:|:--------:|:----------:|:--------:|:-------:|:-------:|:-------------:|:----------:|:-------:|:-------:|:-------:|:--------:|
|     I    |  raht |   jibt   |   qalt  |  šūft  |   zart   |  kant |  matt  |      fatt     |   zaḥt  |  katabt  |   maraqt   |  tarakt  |  qatalt |  sa'alt |     sakatt    |   ṭalabt   |  'aꜧadt |  ḍarabt |  dafaƈt |  daꜧalt  |
|    We    | rahna |   jibna  |  qalna  |  šūfna |   zarna  | kanna |  matna |     fatna     |  zaḥna  |  katabna |   maraqna  |  tarakna | qatalna | sa'alna |    sakatna    |   ṭalabna  | 'aꜧadna | ḍarabna | dafaƈna |  daꜧalna |
|  You (m) |  raht |   jibt   |   qalt  |  šūft  |   zart   |  kant |  matt  |      fatt     |   zaḥt  |  katabt  |   maraqt   |  tarakt  |  qatalt |  sa'alt |     sakatt    |   ṭalabt   |  'aꜧadt |  ḍarabt |  dafaƈt |  daꜧalt  |
|  You (f) | rahti |   jibti  |  qalti  |  šūfti |   zarti  | kanti |  matti |     fatti     |  zaḥti  |  katabti |   maraqti  |  tarakti | qatalti | sa'alti |    sakatti    |   ṭalabti  | 'aꜧadti | ḍarabti | dafaƈti |  daꜧalti |
| You (pl) | rahtu |   jibtu  |  qaltu  |  šūftu |   zartu  | kantu |  mattu |     fattu     |  zaḥtu  |  katabtu |   maraqtu  |  taraktu | qataltu | sa'altu |    sakattu    |   ṭalabtu  | 'aꜧadtu | ḍarabtu | dafaƈtu |  daꜧaltu |
|    He    |  rāḥ  |    jāb   |   qāl   |   šāf  |    zār   |  kān  |   māt  |      fāt      |   zāḥ   |   katab  |    maraq   |   tarak  |  qatal  |  sa'al  |     sakat     |    ṭalab   |  'aꜧad  |  ḍarab  |  dafaƈ  |   daꜧal  |
|    She   | rāḥat |   jābat  |  qālat  |  šāfat |   zārat  | kānat |  mātat |     fātat     |  zāḥat  |  katbat  |   maraqt   |  tarakt  |  qatalt |  sa'alt |     sakatt    |   ṭalabt   |  'aꜧadt |  ḍarabt |  dafaƈt |  daꜧalt  |
|   They   |  rāḥu |   jābu   |   qālu  |  šāfu  |   zāru   |  kānu |  mātu  |      fātu     |   zāḥu  |  katabu  |   maraqu   |  taraku  |  qatalu |  sa'alu |     sakatu    |   ṭalabu   |  'aꜧadu |  ḍarabu |  dafaƈu |  daꜧalu  |