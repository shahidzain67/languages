+++
title = "Lesson 11"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-14"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|         English        | Arabic | Transliteration |
|:----------------------:|:------:|:---------------:|
|         Father         |        |                 |
|       Elementary       |        |                 |
|           Son          |        |                 |
|        Daughter        |        |                 |
|         Brother        |        |                 |
|         Sister         |        |                 |
|         Mother         |        |                 |
|         English        |        |                 |
|          First         |        |                 |
|          Girl          |        |                 |
|          Ninth         |        |                 |
| Differential equations |        |                 |
|         Second         |        |                 |
|          Third         |        |                 |
|         Eighth         |        |                 |
|          Fifth         |        |                 |
|        Academic        |        |                 |
|         Fourth         |        |                 |
|         Husband        |        |                 |
|          Wife          |        |                 |
|         Seventh        |        |                 |
|          Sixth         |        |                 |
|          Tenth         |        |                 |
|         Family         |        |                 |
|      Academic term     |        |                 |
|          Only          |        |                 |
|         Physics        |        |                 |
|        Chemistry       |        |                 |
|           Who          |        |                 |
|          Only          |        |                 |
|           Boy          |        |                 |