+++
title = "Lesson 3"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-01"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary


| English                   	| Arabic     	| Transliteration 	|
|---------------------------	|------------	|-----------------	|
| Fine<br>Well              	| بخير       	| bekhair         	|
| Condition<br>Circumstance 	| حال        	| Haal            	|
| Thank God                 	| الحمد لله  	| Alhumdu lilah   	|
| Good morning              	| صباح الخير 	| Sabaahul khair  	|
| Good morning (response)   	| صباح النور 	| Sabaahul noor   	|
| How                       	| كيف        	| Kaif            	|
| How are you?              	| كيف الحال  	| Kaif al haal    	|