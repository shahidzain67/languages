+++
title = "Lesson 8 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-12"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

|            English           |      Arabic      |    Transliteration   |
|:----------------------------:|:----------------:|:--------------------:|
|      With<br>By means of     |         ب        |          b-          |
|     With<br>Together with    |        مع        |          maƈ         |
|        Fight<br>Brawl        |        طوش       |         ṭōše         |
|       Poor<br>Poor (pl)      | مسكين<br>مساكنين |  miskīn<br>masāknīn  |
|            Present           |       موجود      |        mawjūd        |
|    Storeroom<br>Storerooms   |   مخزن<br>مخازن  |   maꜧzan<br>maꜧāzan  |
|         Week<br>Weeks        |    جُمعَة<br>جُمَع   |    jumƈa<br>jumaƈ    |
|         Week<br>Weeks        |  إِسْبُوع<br>أَسَابِيْع |   'usbūƈ<br>'asābīƈ  |
|            Husband           |        جَوْز       |          jōz         |
|            Machine           |       ماكنة      |   mākina<br>mākana   |
|            Myself            |       لحالي      |        la-ḥāli       |
| Yourself (m)<br>Yourself (f) |       لحالك      | la-ḥālak<br>la-ḥālek |
|       In the same house      |   في نفس البيت   |    fi nafs il-bēt    |
|         The same key         |    نفس المفتاح   |    nafs il-muftāḥ    |
|      In the same colour      |     بنفس الّون    |     b-nafs il-lōn    |
|        The same thing        |     نفس الشّي     |       nafs iššī      |