+++
title = "Lesson 8"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-08"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|             English             |      Arabic     |   Transliteration   |
|:-------------------------------:|:---------------:|:-------------------:|
|  Literature<br>Literature (pl)  |   ادب<br>اداب   |    adab<br>adaab    |
|        Ground<br>Grounds        |   ارض<br>أراضٍ   |   arad<br>araadin   |
|            Those (pl)           |      أولئك      |       'uwlayik      |
|            These (pl)           |      هؤلاء      |        hwla'        |
|          House<br>House         |   بيت<br>بيوت   |    bayt<br>buyut    |
|             Business            |      تجارة      |       tijaara       |
|               That              |       ذلك       |         dhlk        |
|               Law               |    حقّ<br>حقوق   |    huqqa<br>huquq   |
|         Sport<br>Sports         | رياضة<br>رياضات |   riada<br>riadat   |
|             Medicine            |        طبّ       |          tb         |
|             Several             |       عدّة       |        iddatu       |
|       Science<br>Sciences       |   علم<br>علوم   |    eulim<br>eulum   |
|       College<br>Colleges       | كلّيّة<br>الكّليات |   klly<br>alkklyat  |
|      Language<br>Languages      |  لغة<br>اللغات  |  lugha<br>allughat  |
|    Laboratory<br>Laboratories   |  مخبر<br>مخابر  | mukhbir<br>makhabir |
| Swimming pool<br>Swimming pools |  مسبح<br>مسابح  |  musabih<br>musabih |
|        Office<br>Offices        |  مكتب<br>مكاتب  |  maktab<br>makatib  |
|       Library<br>Libraries      | مكتبة<br>مكتبات | maktaba<br>muktabat |
|    Playground<br>Playgrounds    |  ملعب<br>ملاعب  |  maleab<br>malaeib  |
|           Engineering           |      هندسة      |       handasa       |