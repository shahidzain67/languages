+++
title = "Numbers"
description = ""
tags = [
    "BSL"
]
date = "2021-10-19"
categories = [
    "Week 2"
]
menu = "main"
+++

#### Summary
<video width="50%" height="50%" controls loop>
    <source src="/Mya_2.mp4" type="video/mp4">
</video>

#### Numbers 1-10
<video controls loop>
    <source src="/1.mp4" type="video/mp4">
</video>
 <video controls loop>
     <source src="/2.mp4" type="video/mp4">
 </video>
<video controls loop>
    <source src="/3.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/4.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/5.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/6.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/7.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/8.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/9.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/10.mp4" type="video/mp4">
</video>

#### Numbers 11-20
<video controls loop>
    <source src="/11.mp4" type="video/mp4">
</video>
 <video controls loop>
     <source src="/12.mp4" type="video/mp4">
 </video>
<video controls loop>
    <source src="/13.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/14.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/15.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/16.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/17.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/18.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/19.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/20.mp4" type="video/mp4">
</video>

#### Age
<video controls loop>
    <source src="/age.mp4" type="video/mp4">
</video>

#### How old are you?
<video controls loop>
    <source src="/howold.mp4" type="video/mp4">
</video>

#### How are you?
<video controls loop>
    <source src="/how_are_you.mp4" type="video/mp4">
</video>

#### Good
<video controls loop>
    <source src="/good.mp4" type="video/mp4">
</video>

#### Happy
<video controls loop>
    <source src="/happy.mp4" type="video/mp4">
</video>

#### Sad
<video controls loop>
    <source src="/sad.mp4" type="video/mp4">
</video>

#### Angry
<video controls loop>
    <source src="/angry.mp4" type="video/mp4">
</video>

#### Excited
<video controls loop>
    <source src="/excited.mp4" type="video/mp4">
</video>

#### Disappointed
<video controls loop>
    <source src="/disappointed.mp4" type="video/mp4">
</video>

#### Frustrated
<video controls loop>
    <source src="/frustrated.mp4" type="video/mp4">
</video>

#### Jealous
<video controls loop>
    <source src="/jealous.mp4" type="video/mp4">
</video>

#### Tired
<video controls loop>
    <source src="/tired.mp4" type="video/mp4">
</video>

#### Sign (Sign Language)
<video controls loop>
    <source src="/sign.mp4" type="video/mp4">
</video>

#### Slower
<video controls loop>
    <source src="/slow.mp4" type="video/mp4">
</video>

#### Please
<video controls loop>
    <source src="/please.mp4" type="video/mp4">
</video>

#### Thank you
<video controls loop>
    <source src="/thanks.mp4" type="video/mp4">
</video>

#### Sorry
<video controls loop>
    <source src="/sorry.mp4" type="video/mp4">
</video>

#### Slower please
<video controls loop>
    <source src="/slowplease.mp4" type="video/mp4">
</video>

#### Again
<video controls loop>
    <source src="/again.mp4" type="video/mp4">
</video>

#### Sorry again please
<video controls loop>
    <source src="/sorryagainplease.mp4" type="video/mp4">
</video>

#### Nice to meet you
<video controls loop>
    <source src="/nicemeetyou.mp4" type="video/mp4">
</video>