+++
title = "Adjectives"
description = ""
tags = [
    "Spanish"
]
date = "2020-06-26"
categories = [
    "Advanced"
]
menu = "main"
+++


## New Words &emsp;|&emsp; Nuevas Palabras

| English      	| Spanish        	|
|--------------	|----------------	|
| Dying        	| Moribundo      	|
| Preserved     | Disecada          |


## Practice &emsp;|&emsp; Práctica

#### Moribundo | Dying
<details>
    <summary>Para mis sueños moribundos y huecos</summary>
    For my dying and hollow dreams
</details>

---

#### Disecada | Preserved
<details>
    <summary>O ir al museo a ver la ballena disecada.</summary>
    Or go see the stuffed whale at the museum.
</details>