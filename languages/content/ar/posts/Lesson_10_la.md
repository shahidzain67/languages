+++
title = "Lesson 10 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-16"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

| English                                     | Arabic                  | Transliteration           |
|---------------------------------------------|-------------------------|---------------------------|
| Story<br>Stories                            | قصّة<br>قِصَص              | quṣṣa<br>quṣaṣ            |
| Boy<br>Boys                                 | صبا<br>صبيان            | ṣabi<br>ṣibyān            |
| Information, news<br>Information, news (pl) | خبر<br>أخبار            | ꜧabar<br>'aꜧbār           |
| The rest<br>The remainder                   | إل باقي                 | il-bāqi                   |
| Guest (m)                                   | ضيف                     | ḍēf                       |
| Near                                        | قريب من                 | qarīb min                 |
| Relative, relation<br>Relatives, relations  | قريب<br>قرايب           | qarīb<br>qarāyeb          |
| Sweet<br>Beautiful<br>Nice                  | حلو                     | ḥilu                      |
| Friend, owner<br>Friends, owners            | صاحب<br>صحاب            | ṣāḥeb<br>ṣḥāb             |
| Husband                                     | جوز<br>زوج              | jōz<br>zawj               |
| There is<br>There are                       | في                      | fi                        |
| I've got<br>I have<br>At my house<br>By me  | عندي                    | ƈindi                     |
| I've got<br>I have                          | في عندي                 | fi ƈindi                  |
| There was<br>There were                     | كان في                  | kān fi                    |
| I had                                       | كان في عندي<br>كان عندي | kān fi ƈindi<br>kān ƈindi |