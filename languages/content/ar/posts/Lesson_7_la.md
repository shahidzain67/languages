+++
title = "Lesson 7 - LA"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-11"
categories = [
    "Speaking Arabic"
]
menu = "main"
+++

# Vocabulary

|               English              |      Arabic     |  Transliteration  |
|:----------------------------------:|:---------------:|:-----------------:|
|          Father<br>Fathers         |     أب<br>ابّ    |    'ab<br>'abb    |
|              Father of             |        أب       |        'abu       |
|              My father             |       أَبُو       |       'abūy       |
|                Door                |       باب       |        bāb        |
|             Man<br>Men             |   زَلَمِة<br>زْلام  |  zalame<br>zaalam |
|               Bicycle              |      بَسْكَلِيْتّ     |      busuklēt     |
|                That                |     ان<br>انّ    |   'inn<br>'inno   |
|                Which               |        ايّ       | 'ayya<br><br>'ayy |
|                Blue                |       أَزْرَق      |       'azraq      |
|                 Red                |       أَحْمَر      |       'aḥmar      |
|                White               |       أَبْيَض      |       'abyaḍ      |
|             Which one?             |     ايّ واحد     |    'ayya wāḥad    |
|         Brother<br>Brothers        |    اخ<br>اخى    |    'aꜧ<br>'iꜧwe   |
|             Brother of             |        اخ       |        'aꜧu       |
|             My brother             |       اخو       |       'aꜧuy       |
|          Sister<br>Sisters         |   أخت<br>أخوات  |   'uꜧt<br>'ꜧawāt  |
|                 Sun                |       شمس       |        šams       |
|          Colour<br>Colours         |   لون<br>الوان  |    lōn<br>alwān   |
|    That<br>Which<br>Who<br>Whom    |        الّ       |       'illi       |
|               Outside              |       بَرَّا       |       barra       |
|               Inside               |       جُوَّا       |       juwwa       |
| Village headman<br>Village headmen |  مختار<br>مخاتر | muꜧtār<br>maꜧatīr |
|               Beside               |    جَمب<br>حدّ    |    jamb<br>ḥadd   |
|               Kitchen              |       مَطْبَخ      |       maṭbaꜧ      |
|             Key<br>Keys            | مِفْتَاْح<br>مَفَاْتِيْح | muftāḥ<br>mafatīḥ |