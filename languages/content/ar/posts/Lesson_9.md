+++
title = "Lesson 89"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-11"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|              English              |      Arabic     |   Transliteration  |
|:---------------------------------:|:---------------:|:------------------:|
|                God                |       الله      |        allah       |
|               Sorry               |   آسف<br>آسفة   |    asif<br>asifa   |
|           Woman<br>Women          |  النساء<br>نساء |  alnisa'<br>nisa'  |
|              English              |    الإنجليزية   |    al'iinjlizia    |
| Or (statements)<br>Or (questions) |     او<br>ام    |      aw<br>um      |
|                 In                |        في       |         fi         |
|           Card<br>Cards           | بطاقة<br>بطاقات |  bitaqa<br>bitaqat |
|     Small town<br>Small towns     |  بلدة<br>بلدات  |   balda<br>baldat  |
|             Man<br>Men            |   رجل<br>رجال   |    rajl<br>rijal   |
|         Number<br>Numbers         |   رقم<br>ارقام  |   raqm<br>'arqam   |
|                Plus               |       زائد      |        zayid       |
|               Minus               |       ناقص      |        naqis       |
|               Equal               |       ساوى      |        sawaa       |
|              To live              |   سكن<br>يسكن   |   sakn<br>yuskin   |
|             Dormitory             |    سكن الطلاب   |   sakan altullab   |
|               Street              |       شارع      |       sharie       |
|              To want              |   شاء<br>يشاء   |   sha'<br>yasha'   |
|             Apartment             |    شقّة<br>شقق   |  shaqqa<br>shaqaq  |
|               Thanks              |       شكر       |       shakrun      |
|               Arabic              |       عربى      |       eurbaa       |
|              To know              |   عرف<br>يعرف   |   arafa<br>ya'ref  |
|           You're welcome          |       عفواً      |       efwaan       |
|        Address<br>Addresses       | عنوان<br>عناوين | aunwaan<br>eanawin |
|               French              |      فرنسي      |       faransi      |
|           Word<br>Words           |  كلمة<br>كلمات  |  kalima<br>kalimat |
|        How many<br>How much       |        كم       |         kam        |
|                What               |        ما       |         maa        |
|              Meaning              |       معنى      |       maenaa       |
|       Identity<br>Identities      |  هوية<br>هويّات  |    huia<br>hwyat   |
|              Japanese             |    اليابانية    |      alyabania     |