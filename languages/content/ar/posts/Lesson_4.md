+++
title = "Lesson 4"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-02"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

| English               	| Arabic                   	| Transliteration                 	|
|-----------------------	|--------------------------	|---------------------------------	|
| Abu Dhabi             	| أبو ظبي                  	| Aboo Dhaabi                     	|
| Jordan                	| الأردن                   	 | Urdan                           	 |
| Arizona               	| أريزونا                  	| Areezoonaa                      	|
| United Arab Emirates  	| الإمارات العربية المتحدة 	 | al'imarat alearabiat almutahida 	 |
| Indiana               	| إنديانا                  	| 'iindianana                     	|
| Where                 	| أين                      	| 'ayn                            	|
| Baton Rouge           	| باتون روج                	| batun ruj                       	|
| Bahrain               	| البحرين                  	| albahrayn                       	|
| Baghdad               	| بغداد                    	| baghdad                         	|
| Boise                 	| بويزي                    	| buyz                            	|
| Beirut                	| بيروت                    	| Bayrut                          	|
| Tunis                 	| تونس                     	| tunis                           	|
| Algiers               	| الجزائر                  	| aljazayir                       	|
| Djibouti              	| جيبوتي                   	| jibuti                          	|
| Khartoum              	| الخرطوم                  	| alkhartum                       	|
| Damascus              	| دمشق                     	| dimashq                         	|
| Doha                  	| الدوحة                   	| aldawha                         	|
| Rabat                 	| الرباط                   	| alribat                         	|
| Riyadh                	| مدينة الرياض             	| madinat alriyad                 	|
| Saudi Arabia          	| المملكة العربية السعودية 	| almamlakat alearabiat alsaeudia 	|
| Sudan                 	| السودان                  	| alsuwdan                        	|
| Syria                 	| سوريا                    	| suria                           	|
| San'a                 	| صنعاء                    	| sanea'                          	|
| Somalia               	| الصومال                  	| alsuwmal                        	|
| Tripoli               	| طرابلس                   	| tarabulus                       	|
| Iraq                  	| العراق                   	| aleiraq                         	|
| Arab                  	| عرب                      	| earab                           	|
| Amman                 	| عمان                     	| eamman                          	|
| Oman                  	| سلطنة عمان               	| saltanat eamman                 	|
| Fez                   	| فاس                      	| fas                             	|
| Palestine             	| فلسطين                   	| filastin                        	|
| In                    	| في                       	| fi                              	|
| Cairo                 	| القاهرة                  	| alqahr                          	|
| Jerusalem             	| بيت المقدس               	| bayt almuqadas                  	|
| Qatar                 	| دولة قطر                 	| dawlat qatar                    	|
| Kuwait                	| الكويت                   	| alkuayt                         	|
| Lebanon               	| لبنان                    	| lubnan                          	|
| Libya                 	| ليبيا                    	| libia                           	|
| Muscat                	| مسقط                     	| masqat                          	|
| Egypt                 	| مصر                      	| misr                            	|
| Morocco               	| المغرب                   	| almaghrib                       	|
| From, of              	| من                       	| min                             	|
| Manama                	| المنامة                  	| almanama                        	|
| Mauritania            	| موريتانيا                	| muritania                       	|
| Mogadishu             	| مقديشو                   	| maqadishu                       	|
| Nwakshot              	| نواكشوط                  	| nawakshut                       	|
| Homeland<br>Homelands 	| وطن<br>الأوطان           	 | watan<br>al'awtan               	 |
| Wichita               	| ويتشيتا                  	| wayatshita                      	|
| Yemen                 	| اليمن                    	| alyaman                         	|
| Utah                  	| يوتا                     	| yuta                            	|