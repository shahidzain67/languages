+++
title = "Lesson 12"
description = ""
tags = [
    "Arabic"
]
date = "2020-07-20"
categories = [
    "Ahlan wa Sahlan"
]
menu = "main"
+++

# Vocabulary

|      English      | Arabic | Transliteration |
|:-----------------:|:------:|:---------------:|
|       Sorry       |        |                 |
| Most knowledgable |        |                 |
|     Librarian     |        |                 |
|      To speak     |        |                 |
|        Good       |        |                 |
|        Well       |        |                 |
|   Where<br>When   |        |                 |
|    Pharmacology   |        |                 |
|    Knowledgable   |        |                 |
|    Group of ten   |        |                 |
|       Little      |        |                 |
|      A little     |        |                 |
|  To<br>For<br>By  |        |                 |
|      Hundred      |        |                 |