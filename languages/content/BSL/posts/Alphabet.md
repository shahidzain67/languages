+++
title = "Alphabet"
description = ""
tags = [
    "BSL"
]
date = "2021-10-12"
categories = [
    "Week 1"
]
menu = "main"
+++

#### Summary
<video width="50%" height="50%" controls loop>
    <source src="/Mya_1.mp4" type="video/mp4">
</video>

## Left hand side Alphabet

{{<figure src="/alphabet_L.jpeg" position="center" style="border-radius: 8px;">}}

## Right hand side Alphabet

{{<figure src="/alphabet_R.jpeg" position="center" style="border-radius: 8px;">}}

#### Name
<video controls loop>
    <source src="/name.mp4" type="video/mp4">
</video>

#### My name is...
<video controls loop>
    <source src="/my_name_is.mp4" type="video/mp4">
</video>

#### Numbers 1-10
<video controls loop>
    <source src="/1.mp4" type="video/mp4">
</video>
 <video controls loop>
     <source src="/2.mp4" type="video/mp4">
 </video>
<video controls loop>
    <source src="/3.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/4.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/5.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/6.mp4" type="video/mp4">
 </video>
 <video controls loop>
    <source src="/7.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/8.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/9.mp4" type="video/mp4">
</video>
<video controls loop>
    <source src="/10.mp4" type="video/mp4">
</video>