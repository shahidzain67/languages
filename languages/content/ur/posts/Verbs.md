+++
title = "Verbs"
description = ""
tags = [
    "Urdu"
]
date = "2020-06-26"
categories = [
    "Ya Basic"
]
menu = "main"
+++

## New Words &emsp;|&emsp; نئے الفاظ

| English 	| Urdu    	|
|---------	|---------	|
| Use     	| استعمال 	|
| Do      	| کر      	|
| Go      	| جانا    	|
| Come    	| آ       	|
| Laugh   	| ہنسنا   	|
| Be     	| ہونا      	|
| Like   	| پسند کرنا 	|
| Do     	| کرنا      	|

## Practice &emsp;|&emsp; مشق

* میں ٹوٹی ہوئی چمچ استعمال کرتا ہوں

* ہر سال میں اپنے ٹیکس کرتا ہوں

* مجھے بازار جانا پسند ہے

* آؤ مجھے دودھ کی خریداری کرو

* تم بہت ہنستے ہو

* جب میں بڑا ہوں تو میں فائر مین بننا چاہتا ہوں

* کیا آپ کو سبز انڈے اور ہام پسند ہیں؟

* وہ یہ کیسے کرتا ہے

---

1. I use the broken spoon

2. Every year I do my taxes

3. I like to go to the market 

4. Come buy me a milkshake

5. You laugh a lot

6. When I'm older I want to be a fireman

7. Do you like green eggs and ham?
 
8. How does he do it?